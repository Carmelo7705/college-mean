import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

//Components Pages
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SectionComponent } from './section/section.component';
import { LevelComponent } from './level/level.component';
import { TeachersComponent } from './teachers/teachers.component';
import { SubjectsComponent } from './subjects/subjects.component';

//Providers
import { SectionService } from './providers/section-service';
import { LevelService } from './providers/level-service';
import { UserService } from './providers/user.service';
import { StudentsService } from './providers/students.service';
import { ValidationsServiceService } from './providers/validations-service.service';
import { ProfileService } from './providers/profile.service';

import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuard } from './guards/auth.guard';
import { StudentsComponent } from './students/students.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    SectionComponent,
    LevelComponent,
    TeachersComponent,
    SubjectsComponent,
    DashboardComponent,
    StudentsComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [
    SectionService,
    LevelService, 
    UserService, 
    StudentsService, 
    ValidationsServiceService, 
    AuthGuard,
    ProfileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
