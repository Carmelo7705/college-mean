const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const StudentSchema = new Schema({

    name: {
        type: String,
        required: [true, 'El campo Nombre es requerido']
    },

    lastname: {
        type: String,
        required: [true, 'El campo Apellido es requerido']
    },

    ci: {
        type: Number,
        required: [true, 'El campo ci es requerido'],
        unique: [true, 'El campo ci es unico.'],
    },

    age: {
        type: Number,
        required: [true, 'El campo Edad es requerido'],
        min: [12, 'La edad mínima es 12 años.']
    },

    level_id: {
        type: String,
        required: true
    },

    section_id: {
        type: String,
        required: true
    }
});

const Student = module.exports = mongoose.model('Student', StudentSchema);