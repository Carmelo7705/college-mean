const express = require('express');
const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');

require('../config/passport')(passport) // as strategy in ./passport.js needs passport object

var ctrl = require('.././controllers'); //No colocamos el nombre del archivo porque es index.js

//======================== SECTIONS Routes ========================== //

router.get('/sections', ctrl.SectionController.index);

router.get('/levels', ctrl.LevelController.index);

router.post('/students/save', ctrl.StudentController.store);


router.post('/register', ctrl.UserController.register);
//router.get('/exist/:email', ctrl.UserController.verifyEmail);
router.post('/authenticate', ctrl.UserController.auth);
/*router.get('/dashboard', passport.authenticate('jwt', {session: false}), (req, res) => {
    res.json(req.user);
});*/

router.get('/profile', passport.authenticate('jwt', {session: false}), ctrl.UserController.profile);
router.get('/getSectionName/:id', passport.authenticate('jwt', {session: false}), ctrl.UserController.getSectionName );
router.get('/getLevelName/:id', passport.authenticate('jwt', {session: false}), ctrl.UserController.getLevelName );


module.exports = router;

/*
Continuar con las validaciones del formulario de registro. Un toastr para los mensajes de estado, y el login.
*/