const Level = require('../models/level');

module.exports = {
    
    index: function(req, res)
    {
        Level.find({}, (err, data) => {
            res.json(data);
        });
    }
}