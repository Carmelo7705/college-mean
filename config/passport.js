const JwtStrategy = require('passport-jwt').Strategy; //Este es el 2do paso para la auth con JWT.
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('../models/user');

module.exports = (passport) => { // Le pasamos passport como parametro
  let opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt'); //Hay otras, pero esta fue la q funcionó.
  opts.secretOrKey = '123456'; // jwt lo requiere de manera obligatoria, puede ser cualquier cosa.

  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    console.log(jwt_payload); // para visualizar los datos del login en consola y se esten enviando.
    User.getByUserId(jwt_payload.data._id, (err, user) => {
      if(err) {
        return done(err, false);
      }
      if(user) {
        return done(null, user);
      }

      else {
        return done(null, false);
      }
    });
  }));
}