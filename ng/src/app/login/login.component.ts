import { Component, OnInit } from '@angular/core';

import { ValidationsServiceService } from '../providers/validations-service.service';
import { Router } from '@angular/router';
import { UserService } from '../providers/user.service';

declare var toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user = {
    email: '',
    password: '',
  }

  constructor(
    private validationsService: ValidationsServiceService,
    private userService: UserService,
    public router: Router) { }

  ngOnInit() {
    const auth = this.userService.loggedIn();

    if(!auth) {
      this.router.navigate(['dashboard']);
    }
  }

  onSignin() {

    if(!this.validationsService.emptySignin(this.user)) {
      toastr.error('Existen campos vacíos!');
      return false;
    }

    let data = {
      email: this.user.email,
      password: this.user.password
    };

    this.userService.authenticate(data)
      .then(data => {
        if(!data.success) {
          toastr.error(data.msg);
        } else {
          //console.log(data);
          this.userService.storeUserData(data.token, data.user.email, data.user.role);
          this.router.navigate(['dashboard']);
          toastr.success('Bienvenido: ' + data.user.email);
        }
      })
      .catch(err => console.log(err));
  }
}
