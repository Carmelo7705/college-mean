import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Register } from './register';

import { UserService } from '../providers/user.service';
import { StudentsService } from '../providers/students.service';
import { SectionService } from '../providers/section-service';
import { LevelService } from '../providers/level-service';
import { ValidationsServiceService } from '../providers/validations-service.service';

declare var toastr: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  register = new Register;
  sections: any[] = [];
  levels: any[] = [];

  constructor(
    private sectionService: SectionService,
    private levelService: LevelService,
    private userService: UserService,
    private studentService: StudentsService,
    private validationsService: ValidationsServiceService,
    public route: Router) { }

  ngOnInit() {
    this.getSections();
    this.getLevels();

    const auth = this.userService.loggedIn();

    if(!auth) {
      this.route.navigate(['dashboard']);
    }
  }

  signup() {

    if(!this.validationsService.emptyRegister(this.register)) {
      toastr.error('Existen campos vacíos!');
      return false;
    }

    let students = {
      name: this.register.name,
      lastname: this.register.lastname,
      email: this.register.email,
      ci: this.register.ci,
      age: this.register.age,
      level_id: this.register.level_id,
      section_id: this.register.section_id
    };

    //console.log(students);

    this.studentService.store(students)
      .then(data => {

        if(!data.success) {
          toastr.error(data.msg);
        } else {
          let register = {
          email: this.register.email,
          password: this.register.password,
          role: this.register.role,
          student_id: data.data._id
        };

        this.userService.register(register)
          .then(data => {
            toastr.success(data.msg);
            this.route.navigate(['/']);
            this.register = new Register();
          })
          .catch(err => console.log(err));
        }
      })
      .catch(err => console.log(err));
  }

  getSections() {
    this.sectionService.index()
      .then(sections => {
        this.sections = sections;
        this.register.section_id = this.sections[0]._id;
      })
      .catch(err => console.log(err));
  }

  getLevels() {
    this.levelService.index()
      .then(levels => {
        this.levels = levels;
        this.register.level_id = this.levels[0]._id
      })
      .catch(err => console.log(err));
  }

  mayus(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
