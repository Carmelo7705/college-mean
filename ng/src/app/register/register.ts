export class Register {

    constructor(
        //public _id: number = Math.floor(Math.random() * 100),
        public name: string = "",
        public lastname: string = "",
        public ci: number = null,
        public age: number = null,
        public email: string = "",
        public password: string = "",
        public role: number = 1,
        public level_id: string = null,
        public section_id: string = null
    ) {
  
    }
  }
  