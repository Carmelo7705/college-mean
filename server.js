const express = require('express');
const app = express();

const path = require('path');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const http = require('http');
const server = http.createServer(app);
app.set('port', process.env.PORT || 3000); //Si el servidor me da un puerto usamos ese, sino. Usamos el 3000.

const apiRoute = require('./routes/api_v1');

//CORS Middleware
app.use(cors());

//Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/college-db');

//Route Prefix
app.use('/', apiRoute); //Si yo colocara /user, entonces cuando voy a una ruta "/sections" en realidad, sería "/user/sections"

//Static Path
app.use(express.static(path.join(__dirname, 'public')));

server.listen(app.get('port'), () => {
  console.log('server ready on port: ' + app.get('port'));
});

/*app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html')); //Es para ejecutar la web angular desde node, y node reconozca las rutas de angular.
});*/