const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const LevelSchema = new Schema({

    name: {
        type: String,
        required: true
    }

});

const Level = module.exports = mongoose.model('Level', LevelSchema);