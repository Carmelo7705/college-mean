import { Component, OnInit } from '@angular/core';

import { UserService } from '../providers/user.service';
import { ProfileService } from '../providers/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: Object;
  section_name: any;
  level_name: any;

  constructor(private userService: UserService, private profileService: ProfileService) { }

  ngOnInit() {
    this.getMyInfo();
  }

  getMyInfo() {
    this.userService.getProfile()
      .then(data => {
        this.user = data;
        //console.log(this.user);
        this.getSection(this.user[0].student_id.section_id);
        this.getLevel(this.user[0].student_id.level_id);
      })
      .catch(err =>  console.log(err));
  }

  getSection(section_id) {
    this.profileService.getSectionId(section_id)
      .then(data => { 
        this.section_name = data[0].name;
      })
      .catch(err => console.log(err))
  }

  getLevel(level_id) {
    this.profileService.getLevelId(level_id)
      .then(data => { 
        this.level_name = data[0].name;
      })
      .catch(err => console.log(err))
  }

  mayus(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
