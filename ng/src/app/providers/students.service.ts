import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class StudentsService {

  constructor(private _http: Http) { }

  store(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/students/save', data, {headers: headers})
      .map(data => data.json()).toPromise();
  }

}
