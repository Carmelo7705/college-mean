import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  authToken: any;
  user: any;
  role: any;

  constructor(private _http: Http) { }

  register(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/register', data, {headers: headers})
      .map(data => data.json()).toPromise();
  }

  authenticate(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/authenticate', data, {headers: headers})
      .map(data => data.json()).toPromise();
  }

  storeUserData(token, user, role) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', user);
    localStorage.setItem('role', role);

    this.authToken = token;
    this.user = user;
    this.role = role;
  }

  getToken() {
    const token = localStorage.getItem('token');
    this.authToken = token;
  }

  getUser() {
    const user = localStorage.getItem('user');
    const role = localStorage.getItem('role');

    const auth = {
      'email': user,
      'role': role      
    };

    return auth;
  }

  getRole() {
    const role = localStorage.getItem('role');

    return role;
  }

  getProfile() {
    let headers = new Headers();
    this.getToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');

    return this._http.get("http://localhost:3000/profile", {headers: headers })
      .map(data => data.json()).toPromise();
  }

  loggedIn() {
    this.getToken();
    const helper = new JwtHelperService();
    return helper.isTokenExpired(this.authToken);
  }

  logout() {
    this.authToken = null;
    this.user = null;
    this.role = null;

    localStorage.clear();
  }

}
