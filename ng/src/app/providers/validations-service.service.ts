import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ValidationsServiceService {

  constructor(private _http: Http) { }

  emptyRegister(data) {
    if(data.name == "" || data.lastname == "" || data.ci == "" || data.age == "") {
      return false;
    } else {
      return true;
    }
  }

  emptySignin(data) {
    if(data.email == "" || data.password == "") {
      return false;
    } else {
      return true;
    }
  }

  /*existEmailRegister(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

   return this._http.get('http://localhost:3000/exist/' + data.email, {headers: headers})
      .map(data => data.json()).toPromise();
  }*/

}
