const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
    
    email: {
        type: String,
        required: [true, 'El campo Email es requerido'],
        unique: [true, 'El campo email es unico.']
    },

    password: {
        type: String,
        required: [true, 'El campo clave es requerido']
    },

    role: {
        type: Number,
        required: true
    },

    student_id: {
         type: Schema.ObjectId, 
         ref: "Student" 
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getByUserId = (id, callback) => {
    User.findById(id, callback);
}
  
module.exports.addUser = (newUser, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.comparePassword = (psw, hash, callback) => { //psw = actual, hash: De la bd.
    bcrypt.compare(psw, hash, (err, isMatch) => {
        if(err) throw err;

        callback(null, isMatch);
        });
}