import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../providers/user.service';

declare var toastr: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  role: any = null;

  constructor(public userService: UserService, private router: Router) {
    
   }

  ngOnInit() {
    
  }

  logout() {
    this.userService.logout();
    toastr.error('Hasta luego.');
    this.router.navigate(['/']);
    return false;
  }

}
