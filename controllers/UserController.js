const User = require('../models/user');
const Student = require('../models/student');
const Section = require('../models/section');
const Level = require('../models/level');

const jwt = require('jsonwebtoken');

module.exports = {

    register: function(req, res)
    {
        let newUser = new User(req.body);
        User.addUser(newUser, (err, data) => {
            if(err) {
                res.json({success: false, msg: 'Registration failed!'});
                console.log(err);
            } else {
                res.json({success: true, msg: 'Registration Successfully!'});
                console.log(data);
            }
        });
    },

    auth: function(req, res) 
    {
        User.findOne({email: req.body.email}, (err, user) => {
            if(err) throw err;
             if(!user) {
              return res.json({success: false, msg: 'Este email no existe.'});
            }
      
            User.comparePassword(req.body.password, user.password, (err, isMatch) => {
              if(err) throw err;
                if(isMatch) { // Si tanto user, como clave son correctos, empezamos abajo con la creacion del token.
                  const token = jwt.sign({data: user}, '123456', { //Creacion del token único. Y, a partir de aquí se llama a la funcion del passport.
                    expiresIn: 604800 //the session expire in 1 Week.
                  });
      
                  res.json({
                    success: true,
                    token: 'JWT ' + token, //Preferiblemente dejar ese espacio luego de JWT.
                    user: {
                      id:user._id,
                      email: user.email,
                      role: user.role
                    }
                  });
                } else { //Si es isMatch es false, enviamos el mnsj de error.
                  return res.json({success: false, msg: 'Contraseña incorrecta.'});
                }
            });
          });
    },

    profile: function(req, res) 
    {
        //res.json(req.user);

        User.find({_id: req.user._id}, function(err, student) {
            Student.populate(student, {path: "student_id"},function(err, users){
                res.json(users);
              }); 
        });
    },

    getSectionName: function(req, res) {

      Section.find({_id: req.params.id}, (err, data) => {
        res.json(data);
      });

    },

    getLevelName: function(req, res) {

      Level.find({_id: req.params.id}, (err, data) => {
        res.json(data);
      });

    }

}