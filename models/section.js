const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const SectionSchema = new Schema({

    name: {
        type: String,
        required: true
    }
});

const Section = module.exports = mongoose.model('Section', SectionSchema);