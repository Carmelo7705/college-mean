import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class SectionService {

  constructor(private _http: Http) { }

  index() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.get('http://localhost:3000/sections', {headers: headers})
      .map(data => data.json()).toPromise();
  }

}
