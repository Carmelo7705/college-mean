import { TestBed, inject } from '@angular/core/testing';

import { ValidationsServiceService } from './validations-service.service';

describe('ValidationsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidationsServiceService]
    });
  });

  it('should be created', inject([ValidationsServiceService], (service: ValidationsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
