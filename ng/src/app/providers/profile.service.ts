import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { UserService } from './user.service';

import 'rxjs/add/operator/map';

@Injectable()
export class ProfileService {

  constructor(private _http: Http, private userService: UserService)  { }

  getSectionId(section_id) {
    let headers = new Headers();
    this.userService.getToken();
    headers.append('Authorization', this.userService.authToken);
    headers.append('Content-Type', 'application/json');

    return this._http.get("http://localhost:3000/getSectionName/" + section_id, {headers: headers })
      .map(data => data.json()).toPromise();
  }

  getLevelId(level_id) {
    let headers = new Headers();
    this.userService.getToken();
    headers.append('Authorization', this.userService.authToken);
    headers.append('Content-Type', 'application/json');

    return this._http.get("http://localhost:3000/getLevelName/" + level_id, {headers: headers })
      .map(data => data.json()).toPromise();
  }

}
