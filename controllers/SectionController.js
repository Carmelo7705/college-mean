const Section = require('../models/section');

module.exports = {

    index: function(req, res) 
    {
        Section.find({}, (err, data) => {
            res.json(data);
        });
    }
}