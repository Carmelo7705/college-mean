const Student = require('../models/student');
const User = require('../models/user');

let _this = module.exports = {

    store: function(req, res)
    {

        let mail = _this.verifyEmail(req.body.email);
        let cid = _this.verifyCi(req.body.ci);
        
        mail.exec((err, user) => { 
             if(user.length > 0) {
                 res.json({success: false, msg: 'Email Exist!'});
                 return false;
             } else {
                cid.exec((err, user) => {
                    if(user.length > 0) {
                        res.json({success: false, msg: 'Ci Exist!'});
                        return false;
                    } else {
                        delete req.body.email; //Si no existe el email, se puede guardar. Lo eliminamos de aquí.
                        Student.create(req.body, (err, data) => {
                            if(err) {
                                res.json({success: false, msg: err.message});
                                console.log(err);
                            } else {
                                res.json({success: true, msg: 'Registration Successfully!', data: data});
                                console.log(data);
                            }
                        });
                    }
                });
             }
         });
    },

    verifyEmail: function(data)
    {
        let ve = User.find({email: data});
        return ve;
    },

    verifyCi: function(data) 
    {
        let vc = Student.find({ci: data});
        return vc;
    }
}